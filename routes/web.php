<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
Route::resource('portfolio/api', 'Portfolio\Http\Controllers\PortfolioController');
Route::get('/', function () {
    return view('home');
});
Route::get('/portfolio', "Portfolio\Http\Controllers\PortfolioController@index");
Route::get('/portfolio/api', "Portfolio\Http\Controllers\PortfolioController@getdata");
Route::post('/portfolio/api', "Portfolio\Http\Controllers\PortfolioController@simpandata");
Route::put('/portfolio/api', "Portfolio\Http\Controllers\PortfolioController@ubahdata");
Route::delete('/portfolio/api', "Portfolio\Http\Controllers\PortfolioController@destroy");
Route::get("/login","Login\Http\Controllers\LoginController@index");
Route::post("/pross_login","Login\Http\Controllers\LoginController@prosslogin");
Route::get("/logout","Login\Http\Controllers\LoginController@logout");
Route::get('/dashboard', array('as' =>'home', 'uses' => 'Dashboard\Http\Controllers\DashboardController@index'));