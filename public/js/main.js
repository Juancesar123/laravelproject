var mainApp = angular.module("mainApp", ['ngRoute','datatables','checklist-model',"ngResource","ngFileUpload"]);
 mainApp.config(function($routeProvider) {
    $routeProvider
      .when('/portfolio', {
        templateUrl:"portfolio",
        controller:"portfolio"
})
});


 mainApp.factory('socket', ['$rootScope', function($rootScope) {
   var socket = io.connect();

   return {
     on: function(eventName, callback){
       socket.on(eventName, callback);
     },
     emit: function(eventName, data) {
       socket.emit(eventName, data);
     }
   };
 }]);
mainApp.directive('fileModel', ['$parse', function ($parse) {
            return {
               restrict: 'A',
               link: function(scope, element, attrs) {
                  var model = $parse(attrs.fileModel);
                  var modelSetter = model.assign;

                  element.bind('change', function(){
                     scope.$apply(function(){
                        modelSetter(scope, element[0].files[0]);
                     });
                  });
               }
            };
         }]);
mainApp.controller("portfolio",function($scope,$http,DTOptionsBuilder,DTColumnBuilder,Upload,$timeout){
  $scope.dtOptions = DTOptionsBuilder.newOptions()
          .withDisplayLength(5)
          .withOption('bLengthChange', false)
          .withOption('autoWidth', false)
          .withOption('scrollX', false);
          $scope.getdata = function(){
          $http.get("portfolio/api").success(function(data){
            $scope.dataportfolio = data;
          });
      }
      $scope.simpan = function(file){
        var nama = $scope.nama;
        var deskripsi = $scope.deskripsi;
             file.upload = Upload.upload({
      url: 'portfolio/api',
      data: {file: file,nama:nama,deskripsi:deskripsi},
    });

    file.upload.then(function (response) {
      $timeout(function () {
        file.result = response.data;
      });
    }, function (response) {
      if (response.status > 0)
        $scope.errorMsg = response.status + ': ' + response.data;
    }, function (evt) {
      // Math.min is to fix IE which reports 200% sometimes
      file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
    });
        $scope.getdata();
      }
      $scope.edit=function(item){
        $scope.nama = item.nama;
        $scope.deskripsi=item.deskripsi;
        $scope.id = item.id_portfolio;
      }
      $scope.actionedit=function(){
        var nama = $scope.nama;
        var deskripsi = $scope.deskripsi;
        var id = $scope.id;
        $http.put("portfolio/api",{nama:nama,deskripsi:deskripsi,id:id}).success(function(){
          alert("data sukses di ubah");
          $scope.getdata();
        })
      }
      $scope.user={
        hapusdataportfolio:[]
      }
      $scope.hapus=function(item){
        var id = item.id_portfolio;
        $http.delete("portfolio/api/"+id).success(function(){
          alert("data sukses dihapus");
          $scope.getdata()
        })
      }
      $scope.getdata();
})