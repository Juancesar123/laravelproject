<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
class dashboard extends Controller
{
    //
    public function index(Request $request){
    $nama = $request->session()->get("nama");
    if($nama == null){
    	return redirect()->action('login@index');
    }else{
    return View("dashboard")->with("nama",$nama);
}
}
}
