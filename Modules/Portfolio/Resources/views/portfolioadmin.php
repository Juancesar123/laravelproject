<html>
<head>
</head>
<body>
 <section class="content-header">
      <h1>
       Data Portfolio 
        <small>Data Portfolio</small>
      </h1>
      <ol class="breadcrumb">
        <li><i class = "fa fa-database"></i> Master Data</li>
        <li class="active"><i class = "fa fa-users"></i> Data Portfolio</li>
      </ol>
    </section>
    <div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Input portfolio</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
        <label>nama</label>
        <input type="text" class="form-control" ng-model="nama">
      	</div>
         <div class="form-group">
        <label>Gambar</label>
        <input type="file" class="form-control" ngf-select ng-model="file" name="file" ngf-pattern="'image/*'"
    ngf-accept="'image/*'" ngf-max-size="20MB" ngf-min-height="100" 
    ngf-resize="{width: 100, height: 100}"> 
        </div>
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
         <div class="form-group">
        <label>Deskripsi</label>
        <textarea class="form-control" ng-model="deskripsi">
        </textarea>
        </div>
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-primary" ng-click="simpan(file)"><i class="fa fa-send"></i> Kirim</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<div id="myModal1" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Ubah Kelas</h4>
      </div>
      <div class="modal-body">
       <div class="form-group">
        <label>nama</label>
        <input type="text" class="form-control" ng-model="nama">
        </div>
         <div class="form-group">
        <label>Gambar</label>
        <input type="file" class="form-control" ngf-select ng-model="file" name="file" ngf-pattern="'image/*'"
    ngf-accept="'image/*'" ngf-max-size="20MB" ngf-min-height="100" 
    ngf-resize="{width: 100, height: 100}"> 
        </div>
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
         <div class="form-group">
        <label>Deskripsi</label>
        <textarea class="form-control" ng-model="deskripsi">
        </textarea>
        </div>
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-success" ng-click="actionedit()"><i class="fa fa-send"></i> Kirim</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
    <div class="box box-primary">
<div class="box-header">
<button class="btn btn-primary" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i> Tambah Portfolio</button>
</div>
<div class="box-body">
<table datatable="ng" dt-columns="dtColumns" dt-options="dtOptions" class="table table-bordered table-striped">
<thead>
<th><input type="checkbox" ng-click="checkall()"></th>
<th>nama</th>
<th>gambar</th>
<th>deskripsi</th>
<th>Action</th>
</thead>
<tbody>
<tr ng-repeat="item in dataportfolio">
 <td><input type="checkbox"  checklist-model="user.hapusdataportfolio" checklist-value="item.id_portfolio" role></td>{{user.hapusdataportfolio}}
<td>{{item.nama}}</td>
<td><img ng-src="{{item.gambar}}" width="100" height="100"></td>
<td>{{item.deskripsi}}</td>
<td><button class="btn btn-success" ng-click="edit(item)" data-toggle="modal" data-target="#myModal1"><i class="fa fa-edit"></i> Edit</button><button class="btn btn-danger" ng-click="hapus(item)"><i class="fa fa-trash"></i> Hapus</button></td>
</tr>
</tbody>
</table>
</div>
</div>
</body>
</html>
