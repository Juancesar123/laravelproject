<?php

namespace Modules\Portfolio\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class PortfolioController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
     public function index(){
        return view("portfolio::portfolioadmin");
    }
    public function getdata(){
        $data = DB::table('portfolio')->get();
        echo json_encode($data);
    }
    public function simpandata(Request $request){
        $file = $request->file('file');
        echo "file name is: ".$file->getClientOriginalName();
        echo "file name is: ".$file->getRealPath();
        $destinationfile = "uploads";
        $file->move($destinationfile,$file->getClientOriginalName());
        $file_path = "uploads/".$file->getClientOriginalName();
        $data = array(
            "nama" => $request->input("nama"),
            "deskripsi"=> $request->input("deskripsi"),
            "gambar" => $file_path
            );
        DB::table('portfolio')->insert($data);
    }
    public function ubahdata(Request $request){
        $data = array(
            "nama" => $request->input("nama"),
            "deskripsi"=> $request->input("deskripsi")
            );
        DB::table('portfolio')
            ->where('id_portfolio', $request->input("id"))
            ->update($data);
    }
    public function destroy($id){
        DB::table('portfolio')->where('id_portfolio', $id)->delete();
    }
   
}
