<?php

namespace Modules\Login\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Routing\Redirector;
class LoginController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
   public function index(){
      return view("login::login");
   }
   public function prosslogin(Request $request){
        $data["datauser"] = DB::table("users")->where('username',$request->input("username"))->where('password',md5($request->input("password")))->get();
        if(count($data) == 1){
            $user = $data["datauser"][0]->nama;
            $request->session()->put("nama",$user);
            return redirect()->action('Dashboard\Http\Controllers\DashboardController@index');
             //Auth::login($user);
            //return redirect()->intended('dashboard');
        }else{
            echo"heloo mblo";
        }
    }
    public function logout(Request $request){
        $request->session()->flush();
        return redirect()->action('Login\Http\Controllers\LoginController@index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('login::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('login::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
